\contentsline {chapter}{Contents}{i}{dummy.1}
\contentsline {chapter}{Abstract}{ii}{dummy.3}
\vspace {1em}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.4}
\contentsline {section}{\numberline {1.1}Introduction}{1}{section.5}
\contentsline {section}{\numberline {1.2}Presentation of the company}{1}{section.6}
\contentsline {chapter}{\numberline {2}Tasks and Assignments}{3}{chapter.7}
\contentsline {section}{\numberline {2.1}Analysis of the existing}{3}{section.8}
\contentsline {section}{\numberline {2.2}Choice of the solution}{4}{section.9}
\contentsline {section}{\numberline {2.3}Realizing the solution}{5}{section.10}
\contentsline {section}{\numberline {2.4}Other parallel tasks}{8}{section.42}
\contentsline {chapter}{\numberline {3}Overall reflection on the practical placement}{9}{chapter.43}
\contentsline {section}{\numberline {3.1}Professional}{9}{section.44}
\contentsline {section}{\numberline {3.2}Academic}{10}{section.45}
\contentsline {section}{\numberline {3.3}Personal}{11}{section.46}
\contentsline {chapter}{\numberline {4}Recommendations}{13}{chapter.47}
\contentsline {section}{\numberline {4.1}Orange Business Services}{13}{section.48}
\contentsline {section}{\numberline {4.2}VIA University College}{14}{section.49}
\contentsline {chapter}{\numberline {5}Conclusion}{15}{chapter.50}
\vspace {2em}
\contentsline {chapter}{\numberline {A}Analysis of Whatoo}{17}{appendix.51}
\contentsline {chapter}{\numberline {B}Architecture of Whatoo}{18}{appendix.53}
\vspace {2em}
